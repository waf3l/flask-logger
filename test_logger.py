from flask import Flask
from flask_logger import FlaskLogger
from shutil import rmtree

import os
import unittest
import coverage

cov = coverage.coverage()
cov.start()

app = Flask(__name__)


class SetupTestFlaskLogger(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cov.stop()
        cov.report(include='flask_logger/__init__.py')

    def setUp(self):
        app.config['LOGGING'] = {}

        app.config['LOGGING']['log_path'] = './log'
        app.config['LOGGING']['logging_level'] = 'DEBUG'
        app.config['LOGGING']['logging_file_prefix'] = 'test_logger'
        app.config['LOGGING']['logging_to_file'] = True
        app.config['LOGGING']['logging_to_stdout'] = True
        app.config['LOGGING']['logging_to_db'] = False

    def tearDown(self):
        if os.path.exists('./log'):
            rmtree('./log')


class TestFlaskLogger(SetupTestFlaskLogger):

    def test_create(self):
        logger = FlaskLogger()
        self.assertIsInstance(logger, FlaskLogger)

    def test_create_with_app(self):
        logger = FlaskLogger(app)
        self.assertIsInstance(logger, FlaskLogger)

    def test_init_app(self):
        logger = FlaskLogger()
        logger.init_app(app)
        self.assertTrue(logger.log_path == './log')


if __name__ == '__main__':
    unittest.main()
