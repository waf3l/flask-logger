# -*- coding: utf-8 -*-
"""
Flask-Logger
------------

Flask extension for logging.
"""
from setuptools import setup


setup(
    name='Flask-Logger',
    version='2.0.0',
    url='',
    license='MIT',
    author='Marcin Wojtysiak',
    author_email='wojtysiak.marcin@gmail.com',
    description='Flask extension for logging.',
    long_description=__doc__,
    packages=['flask_logger'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[        
        'Flask>=0.10',
        'Flask-SQLAlchemy>=2.0'
    ],
    tests_require=[
        'coverage'
    ],
    test_suite='test_logger',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
