# -*- coding: utf-8 -*-
"""
Flask extension for logging.

Made by Marcin Wojtysiak 2014
"""

from flask_sqlalchemy import SQLAlchemy
from logging.handlers import RotatingFileHandler
from datetime import datetime

import logging
import os
import sys


class CreateLoggerError(Exception):
    pass


class DBLogger(logging.Handler):

    def __init__(self, db, model):
        """Init logging database handler

        Args:
            db (SQLAlchemy.db.instance): flask-sqlalchemy database instance
            model (SQLAlchemy.db.model): flask-sqlalchemy db model of logging table
        """
        logging.Handler.__init__(self)

        # database instance
        self.db = db

        # declare logger model
        self.model = model

    def emit(self, record):
        """Insert emitted record into db"""

        try:
            log_entry = self.model(record)
            self.db.session.add(log_entry)
            self.db.session.commit()

        except Exception, e:
            self.db.session.rollback()
            print 'Flask-Logger log to db error {0}'.format(repr(e))


class FlaskLogger(object):
    """
    Main logger for application
    """

    def __init__(self, app=None, model=None):
        """
        Initialize
        """

        # set config data for setup logger
        self.log_path = None
        self.logging_file_prefix = None
        self.logging_level = None
        self.logging_to_file = None
        self.logging_to_stdout = None
        self.logging_to_db = None

        # log date
        self.date_log = None

        # Log file name for debug logging level
        self.file_log_name = None

        # setup format of log entry
        self.formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)d - %(message)s'
        )

        # database instance local
        self.db = None

        if app is not None:
            self.init_app(app, model)

    def init_app(self, app, model=None):
        """
        Init app
        """
        # set config data for setup logger
        self.log_path = app.config['LOGGING']['log_path']
        self.logging_file_prefix = app.config['LOGGING']['logging_file_prefix']
        self.logging_level = logging.getLevelName(app.config['LOGGING']['logging_level'])
        self.logging_to_file = app.config['LOGGING']['logging_to_file']
        self.logging_to_stdout = app.config['LOGGING']['logging_to_stdout']
        self.logging_to_db = app.config['LOGGING']['logging_to_db']

        # log date
        self.date_log = datetime.now().strftime('%Y%m%d')

        # Log file name for debug logging level
        self.file_log_name = '{0}_{1}.log'.format(self.logging_file_prefix, self.date_log)

        # set application log level
        app.logger.setLevel(self.logging_level)

        # if we want to log to db we need model
        if model is not None:
            # create db object instance
            self.db = SQLAlchemy(app)

        # configure logger
        self._create_logger(app, model)

    def _create_logger(self, app, model):
        """
        Configure and init logger
        """
        try:

            #  prepare stdout logger if enabled
            if self.logging_to_stdout:
                # stream handler
                handler_stream = logging.StreamHandler(stream=sys.stdout)
                handler_stream.setLevel(self.logging_level)
                handler_stream.setFormatter(self.formatter)

                # register new log handler
                app.logger.addHandler(handler_stream)

            # prepare logger to file if is enabled
            if self.logging_to_file:

                # check if log directory exist
                if not os.path.isdir(os.path.join(self.log_path)):

                    # create log directory
                    os.makedirs(os.path.join(self.log_path), mode=0770)

                # prepare file handler
                path_file_handler = os.path.join(self.log_path, self.file_log_name)
                handler_file = RotatingFileHandler(path_file_handler, maxBytes=8192000, backupCount=100)
                handler_file.setLevel(self.logging_level)
                handler_file.setFormatter(self.formatter)

                # register new log handler
                app.logger.addHandler(handler_file)

            # prepare logger to db if is enabled
            if self.logging_to_db and model is not None and self.db is not None:
                with app.app_context():
                    # prepare db log handler
                    db_handler = DBLogger(self.db, model)
                    db_handler.setLevel(self.logging_level)
                    db_handler.setFormatter(self.formatter)

                    # register new log handler
                    app.logger.addHandler(db_handler)

        except Exception, e:
            raise CreateLoggerError(e)
